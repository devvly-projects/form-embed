<?php

namespace Bolt\Extension\Devvly\FormEmbed;

use Bolt\Extension\DatabaseSchemaTrait;
use Bolt\Extension\Devvly\FormEmbed\Field\FormEmbedFieldType;
use Bolt\Extension\SimpleExtension;

/**
 * FormEmbed extension class.
 *
 * @author Devvly <info@devvly.com>
 */
class Extension extends SimpleExtension
{
    use DatabaseSchemaTrait;


    /**
     * @inheritDoc
     */
    public function registerFields()
    {
        return [
          new FormEmbedFieldType(),
        ];
    }

    /**
     * @inheritDoc
     */
    protected function registerTwigPaths()
    {

        return [
          'templates/bolt' => ['namespace'=>'bolt','position' => 'prepend'],
        ];
    }

}
