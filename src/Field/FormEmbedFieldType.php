<?php


namespace Bolt\Extension\Devvly\FormEmbed\Field;

use Bolt\Storage\EntityManager;
use Bolt\Storage\Field\Type\FieldTypeBase;
use Bolt\Storage\QuerySet;
use Doctrine\DBAL\Types\Type;

class FormEmbedFieldType extends FieldTypeBase
{
    /**
     * @inheritDoc
     */
    public function getName()
    {
        return "formembed";
    }

    public function persist(QuerySet $queries, $entity, EntityManager $em = null)
    {
        $key = $this->mapping['fieldname'];
        $qb = $queries->getPrimary();
        $value = $entity->get($key);
        if ($this->isJson($value)) {
            $phpVal = json_decode($value,TRUE);
            if(isset($phpVal['form_name']) && !empty($phpVal['form_name'])){
                $qb->setValue($key, ':' . $key);
                $qb->set($key, ':' . $key);
                $qb->setParameter($key, (string)$value);
            }
        }
        // If the user is trying update to empty value:
        elseif ($value === null){
            $qb->setValue($key, ':' . $key);
            $qb->set($key, ':' . $key);
            $qb->setParameter($key, '');
        }
    }

    /**
     * @inheritDoc
     */
    public function getStorageType()
    {
        return Type::getType('json');
    }
}