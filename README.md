Form Embed
======================
##### Description:
Provides the ability to embed forms inside entities.
##### Usage:

1. ##### Add the form to a content type:
    ~~~
    entries:
        name: Entries
        singular_name: Entry
        fields:
            form:
                type: formembed
                group: content
                label: "Form"
                default: >
                    {
                        "form_name": "",
                        "form_label": ""
                    }
    ~~~
1.  ##### Display it in a template:
    for example in file `public/theme/YOUR_THEME/partials/_sub_field_blocks.twig`
    ~~~
     {% if fieldtype == "formembed" and value is not empty %}
            {{ boltforms(form_data.form_name) }}
     {% endif %}
    ~~~
    
